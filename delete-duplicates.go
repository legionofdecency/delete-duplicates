// Package main is the entry point for deleteing duplicate files from  the specified directory.
package main

import (
	"crypto/md5"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
	"strings"
)

type filesBySize map[int64][]*FileDetails

var (
	dirs        string
	compareSize int64
	interactive bool
)

type FileDetails struct {
	Path string
	Size int64
}

func init() {
	flag.StringVar(&dirs, "dirs", "", "Directory or comma separated directories from which to delete duplicate files.")
	flag.Int64Var(&compareSize, "compare_size", 0, "Number of bytes used to compare files.  0 (default) uses the entire file.")
	flag.BoolVar(&interactive, "interactive", false, "Review and approve each removal interactively?")
}

func readContents(d string) (filesBySize, error) {
	directories := strings.Split(d, ",")
	ret := make(filesBySize)
	for _, directory := range directories {
		directory = strings.Trim(directory, " ")
		files, err := ioutil.ReadDir(directory)
		counter := 0
		if err != nil {
			return nil, err
		}
		for _, file := range files {
			if !file.Mode().IsRegular() {
				continue
			}
			details := &FileDetails{
				Path: filepath.Join(directory, file.Name()),
				Size: file.Size(),
			}
			_, ok := ret[details.Size]
			if !ok {
				ret[details.Size] = []*FileDetails{details}
			} else {
				ret[details.Size] = append(ret[details.Size], details)
			}
			counter++
		}
		fmt.Printf("Analyzing %d files in %q\n", counter, directory)
	}
	overlap := 0
	for _, files := range ret {
		if len(files) > 1 {
			overlap += len(files)
		}
	}
	if overlap > 0 {
		fmt.Printf("Analyzing %d files with same sizes.\n", overlap)
	} else {
		fmt.Println("No files had overlapping sizes.  Done.")
	}

	return ret, nil
}

func getMD5Sum(path string) (string, error) {
	f, err := os.Open(path)
	if err != nil {
		return "", err
	}
	defer f.Close()

	h := md5.New()
	if compareSize == 0 {
		if _, err := io.Copy(h, f); err != nil {
			return "", err
		}
	} else {
		if _, err := io.CopyN(h, f, compareSize); err != nil {
			switch err {
			case io.EOF:
				f.Seek(0, 0)
				if _, err := io.Copy(h, f); err != nil {
					return "", err
				}
			default:
				return "", err
			}
		}
	}

	return fmt.Sprintf("%x", h.Sum(nil)), nil
}

func remove(file *FileDetails) error {
	if err := os.Remove(file.Path); err != nil {
		return fmt.Errorf("Failed to remove %q: %s", file.Path, err)
	}
	fmt.Printf("Deleted %q: %dMB\n", filepath.Base(file.Path), file.Size/1024/1024)
	return nil
}

func getApprovalToDelete(path string) bool {
	var input string
	for {
		fmt.Printf("Ok to delete %q?: (y/n) ", path)
		fmt.Scanln(&input)
		switch t := strings.TrimSpace(input); t {
		case "y":
			return true
		case "n":
			return false
		default:
			fmt.Println("Please enter y or n followed by enter.")
			continue
		}
	}
}

func main() {
	flag.Parse()
	if dirs == "" {
		panic("Must specify --dirs")
	}
	if compareSize != 0 {
		fmt.Printf("Using the first %d bytes for comparison\n", compareSize)
	}
	bySize, err := readContents(dirs)
	if err != nil {
		panic(fmt.Sprintf("Failed to read directory %q contents: %s", dirs, err))
	}

	for _, files := range bySize {
		if len(files) > 1 {
			byHash := make(map[string][]*FileDetails)
			for _, file := range files {
				sum, err := getMD5Sum(file.Path)
				if err != nil {
					panic(fmt.Sprintf("Failed to read %q: %s", file.Path, err))
				}
				_, ok := byHash[sum]
				if !ok {
					byHash[sum] = []*FileDetails{file}
				} else {
					byHash[sum] = append(byHash[sum], file)
				}
			}
			for _, sameHashFiles := range byHash {
				if len(sameHashFiles) > 1 {
					sort.Slice(sameHashFiles, func(i, j int) bool { return len(sameHashFiles[i].Path) < len(sameHashFiles[j].Path) })
					fmt.Println("The following files are the same:")
					for _, file := range sameHashFiles {
						fmt.Println(file.Path)
					}
					for _, file := range sameHashFiles[1:] {
						if !interactive || (interactive && getApprovalToDelete(file.Path)) {
							if err := remove(file); err != nil {
								panic(err)
							}
						}
					}
				}
			}
		}
	}
}
